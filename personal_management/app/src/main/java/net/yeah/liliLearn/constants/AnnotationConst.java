package net.yeah.liliLearn.constants;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class AnnotationConst {

    @IntDef({Const.TaskCategory.emergency, Const.TaskCategory.important, Const.TaskCategory.ordinary})
    @Retention(RetentionPolicy.SOURCE)
    public @interface TaskCategory {
        // no-op
    }

    @IntDef({Const.TaskState.all, Const.TaskState.finished, Const.TaskState.unFinished})
    @Retention(RetentionPolicy.SOURCE)
    public @interface TaskState {
        // no-op
    }
}
