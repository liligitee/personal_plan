package net.yeah.liliLearn.constants;

public final class Const {
    public static final class TaskCategory {
        public static final int emergency = 1;// 紧急
        public static final int important = 2;// 重要
        public static final int ordinary = 3;// 普通
    }

    public static final class TaskState {
        public static final int all = 0;// 完成
        public static final int finished = 1;// 完成
        public static final int unFinished = 2;// 未完成
    }
}
