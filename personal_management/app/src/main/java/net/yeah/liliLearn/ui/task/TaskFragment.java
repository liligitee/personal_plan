package net.yeah.liliLearn.ui.task;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import net.yeah.liliLearn.R;
import net.yeah.liliLearn.base.BaseFragment;
import net.yeah.liliLearn.constants.AnnotationConst;
import net.yeah.liliLearn.constants.Const;
import net.yeah.liliLearn.entity.Task;
import net.yeah.liliLearn.eventbus.RefreshTask;
import net.yeah.liliLearn.greendao.TaskDao;
import net.yeah.liliLearn.ui.task.activity.SaveTaskActivity;
import net.yeah.liliLearn.ui.task.activity.TaskListActivity;
import net.yeah.liliLearn.ui.task.adapter.TaskCardAdapter;
import net.yeah.liliLearn.utils.DBManager;
import net.yeah.liliLearn.widget.TaskCardView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class TaskFragment extends BaseFragment {
    private static final String TAG = "TaskFragment";

    private TaskCardView mEmergencyCardView;
    private TaskCardView mImportantCardView;
    private TaskCardView mOrdinaryCardView;
    private FloatingActionButton mFab;

    private TaskCardAdapter taskCardAdapter;
    private TaskCardAdapter importantTaskAdapter;
    private TaskCardAdapter ordinaryTaskAdapter;
    private List<Task> emergencyTasks = new ArrayList<>();
    private List<Task> importantTasks = new ArrayList<>();
    private List<Task> ordinaryTasks = new ArrayList<>();

    public static TaskFragment newInstance() {
        Bundle args = new Bundle();
        TaskFragment fragment = new TaskFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_task, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        initView(view);
        setHasOptionsMenu(true);
        initData();
        setEvent();
    }

    private void initView(View view) {
        mEmergencyCardView = view.findViewById(R.id.emergency_card_view);
        mImportantCardView = view.findViewById(R.id.important_card_view);
        mOrdinaryCardView = view.findViewById(R.id.ordinary_card_view);
        mFab = view.findViewById(R.id.fab);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_info) {
            startActivity(new Intent(getContext(), TaskListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initData() {
        taskCardAdapter = new TaskCardAdapter(getContext(), emergencyTasks);
        mEmergencyCardView.setAdapter(taskCardAdapter);

        importantTaskAdapter = new TaskCardAdapter(getContext(), importantTasks);
        mImportantCardView.setAdapter(importantTaskAdapter);

        ordinaryTaskAdapter = new TaskCardAdapter(getContext(), ordinaryTasks);
        mOrdinaryCardView.setAdapter(ordinaryTaskAdapter);

        emergencyTasks.addAll(fetchTasksByCategory(Const.TaskCategory.emergency));
        taskCardAdapter.notifyDataSetChanged();
        importantTasks.addAll(fetchTasksByCategory(Const.TaskCategory.important));
        importantTaskAdapter.notifyDataSetChanged();
        ordinaryTasks.addAll(fetchTasksByCategory(Const.TaskCategory.ordinary));
        ordinaryTaskAdapter.notifyDataSetChanged();

    }

    private List<Task> fetchTasksByCategory(@AnnotationConst.TaskCategory int category) {
        return DBManager.getInstance()
                .getDaoSession()
                .getTaskDao()
                .queryBuilder()
                .where(TaskDao.Properties.Category.eq(category), TaskDao.Properties.State.eq(Const.TaskState.unFinished))
                .orderDesc(TaskDao.Properties.UpdateTime)
                .build()
                .list();
    }

    private void setEvent() {
        mFab.setOnClickListener(v -> startActivity(new Intent(getContext(), SaveTaskActivity.class)));
        mEmergencyCardView.setEmptyViewClickListener("添加", v -> startActivity(new Intent(getContext(), SaveTaskActivity.class)));
        mImportantCardView.setEmptyViewClickListener("添加", v -> startActivity(new Intent(getContext(), SaveTaskActivity.class)));
        mOrdinaryCardView.setEmptyViewClickListener("添加", v -> startActivity(new Intent(getContext(), SaveTaskActivity.class)));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void EventBusRefreshTask(RefreshTask refreshTask) {
        switch (refreshTask.category) {
            case Const.TaskCategory.emergency:
                emergencyTasks.clear();
                emergencyTasks.addAll(fetchTasksByCategory(Const.TaskCategory.emergency));
                taskCardAdapter.notifyDataSetChanged();
                break;
            case Const.TaskCategory.important:
                importantTasks.clear();
                importantTasks.addAll(fetchTasksByCategory(Const.TaskCategory.important));
                importantTaskAdapter.notifyDataSetChanged();
                break;
            case Const.TaskCategory.ordinary:
                ordinaryTasks.clear();
                ordinaryTasks.addAll(fetchTasksByCategory(Const.TaskCategory.ordinary));
                ordinaryTaskAdapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }
}
