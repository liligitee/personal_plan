package net.yeah.liliLearn.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import net.yeah.liliLearn.R;
import net.yeah.liliLearn.ui.home.HomeFragment;
import net.yeah.liliLearn.ui.memo.MemoFragment;
import net.yeah.liliLearn.ui.schedule.ScheduleFragment;
import net.yeah.liliLearn.ui.task.TaskFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, HomeFragment.newInstance())
                .commitNow();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Toolbar toolbar = findViewById(R.id.toolbar);

        if (id == R.id.nav_home) {
            toolbar.setTitle(R.string.menu_item_home);
            replaceFragment(HomeFragment.newInstance());
        } else if (id == R.id.nav_task) {
            toolbar.setTitle(R.string.title_todo_task);
            replaceFragment(TaskFragment.newInstance());
        } else if (id == R.id.nav_month_remind) {
            toolbar.setTitle(R.string.menu_item_month_remind);
            replaceFragment(ScheduleFragment.newInstance());
        } else if (id == R.id.nav_memo) {
            toolbar.setTitle(R.string.menu_item_memo);
            replaceFragment(MemoFragment.newInstance());
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_about) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void replaceFragment(Fragment fragment) {
        View container = findViewById(R.id.fragment_container);
        if (container == null) {
            throw new NullPointerException("not find container!");
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.commit();
    }
}
