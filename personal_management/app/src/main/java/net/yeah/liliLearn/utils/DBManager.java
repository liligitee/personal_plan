package net.yeah.liliLearn.utils;

import android.content.Context;

import net.yeah.liliLearn.base.App;
import net.yeah.liliLearn.greendao.DaoMaster;
import net.yeah.liliLearn.greendao.DaoSession;

public final class DBManager {
    private final static String dbName = "upward_db";
    private static DBManager mInstance;
    private DaoMaster.DevOpenHelper openHelper;
    private static DaoMaster daoMaster;
    private static DaoSession daoSession;
    private Context context;

    public DBManager(Context context) {
        this.context = context;
        openHelper = new DaoMaster.DevOpenHelper(context, dbName, null);
        daoMaster = new DaoMaster(openHelper.getWritableDatabase());
    }

    /**
     * 获取单例引用
     *
     * @return
     */
    public static DBManager getInstance() {
        if (mInstance == null) {
            synchronized (DBManager.class) {
                if (mInstance == null) {
                    mInstance = new DBManager(App.getInstance());
                }
            }
        }
        return mInstance;
    }

    /**
     * 取得DaoSession
     *
     * @return
     */
    public DaoSession getDaoSession() {
        if (daoSession == null) {
            daoSession = daoMaster.newSession();
        }
        return daoSession;
    }
}
