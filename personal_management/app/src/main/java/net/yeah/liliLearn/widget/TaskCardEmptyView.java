package net.yeah.liliLearn.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.yeah.liliLearn.R;

public class TaskCardEmptyView extends LinearLayout {
    private TextView mEmptyText;
    private TextView mJumpButton;
    private OnClickListener listener;

    public TaskCardEmptyView(Context context) {
        this(context, null);
    }

    public TaskCardEmptyView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TaskCardEmptyView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        View view = LayoutInflater.from(context).inflate(R.layout.view_task_card_empty_layout, this);
        initView(view);
    }

    private void initView(View view) {
        mEmptyText = view.findViewById(R.id.empty_text);
        mJumpButton = view.findViewById(R.id.jump_button);
    }

    public void setEmptyText(CharSequence emptyText) {
        if (!TextUtils.isEmpty(emptyText)) {
            mEmptyText.setText(emptyText);
        }
    }

    public void setOnAddClickListener(String title, OnClickListener listener) {
        if (listener != null) {
            mJumpButton.setText(title);
            mJumpButton.setVisibility(VISIBLE);
            mJumpButton.setOnClickListener(listener);
        }
    }
}
