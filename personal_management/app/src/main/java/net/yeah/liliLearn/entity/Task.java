package net.yeah.liliLearn.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class Task {
    /**
     * 任务Id
     */
    @Id(autoincrement = true)
    private Long taskId;
    /**
     * 任务标题
     */
    @NotNull
    private String taskTitle;
    /**
     * 任务描述
     */
    @NotNull
    private String taskDescribe;
    /**
     * 任务类别 紧急 重要 普通
     */
    @NotNull
    private int category;
    /**
     * 任务状态 已完成 未完成
     */
    private int state;
    /**
     * 更新时间
     */
    @NotNull
    private Long updateTime;
    @Generated(hash = 504788719)
    public Task(Long taskId, @NotNull String taskTitle,
            @NotNull String taskDescribe, int category, int state,
            @NotNull Long updateTime) {
        this.taskId = taskId;
        this.taskTitle = taskTitle;
        this.taskDescribe = taskDescribe;
        this.category = category;
        this.state = state;
        this.updateTime = updateTime;
    }
    @Generated(hash = 733837707)
    public Task() {
    }
    public Long getTaskId() {
        return this.taskId;
    }
    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }
    public String getTaskTitle() {
        return this.taskTitle;
    }
    public void setTaskTitle(String taskTitle) {
        this.taskTitle = taskTitle;
    }
    public String getTaskDescribe() {
        return this.taskDescribe;
    }
    public void setTaskDescribe(String taskDescribe) {
        this.taskDescribe = taskDescribe;
    }
    public int getCategory() {
        return this.category;
    }
    public void setCategory(int category) {
        this.category = category;
    }
    public int getState() {
        return this.state;
    }
    public void setState(int state) {
        this.state = state;
    }
    public Long getUpdateTime() {
        return this.updateTime;
    }
    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }


    @Override
    public String toString() {
        return "Task{" +
                "taskId=" + taskId +
                ", taskTitle='" + taskTitle + '\'' +
                ", taskDescribe='" + taskDescribe + '\'' +
                ", category=" + category +
                ", state=" + state +
                ", updateTime=" + updateTime +
                '}';
    }
}
