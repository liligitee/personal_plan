package net.yeah.liliLearn.ui.task.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import net.yeah.liliLearn.R;
import net.yeah.liliLearn.base.BaseActivity;
import net.yeah.liliLearn.constants.Const;
import net.yeah.liliLearn.entity.Task;
import net.yeah.liliLearn.eventbus.RefreshTask;
import net.yeah.liliLearn.utils.DBManager;
import net.yeah.liliLearn.widget.CleanableEditText;

import org.greenrobot.eventbus.EventBus;

public class SaveTaskActivity extends BaseActivity {
    private CleanableEditText mTaskTitle;
    private EditText mTaskDescribe;
    private Button mSubmitButton;
    private RadioGroup mCategoryRadioGroup;
    private TextView mTitleCounter;
    private TextView mDescribeCounter;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_task);
        initView();
        setEvent();
    }

    private void initView() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mTaskTitle = findViewById(R.id.task_title);
        mTaskDescribe = findViewById(R.id.task_describe);
        mSubmitButton = findViewById(R.id.submit_button);
        mTaskTitle = findViewById(R.id.task_title);
        mTaskDescribe = findViewById(R.id.task_describe);
        mCategoryRadioGroup = findViewById(R.id.category_radio_group);
        mSubmitButton = findViewById(R.id.submit_button);
        mTitleCounter = findViewById(R.id.title_counter);
        mDescribeCounter = findViewById(R.id.describe_counter);

    }

    private void setEvent() {
        mSubmitButton.setOnClickListener(v -> validationData());
        toolbar.setNavigationOnClickListener(v -> finish());
        mTaskTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mTitleCounter.setText(s.length() + "/20");
            }
        });
        mTaskDescribe.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // no-op
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // no-op
            }

            @Override
            public void afterTextChanged(Editable s) {
                mDescribeCounter.setText(s.length() + "/100");
            }
        });
    }

    private void validationData() {
        String title = mTaskTitle.getText().toString();
        String describe = mTaskDescribe.getText().toString();
        if (TextUtils.isEmpty(title)) {
            Toast.makeText(this, "请输入任务标题", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(describe)) {
            Toast.makeText(this, "请输入任务描述", Toast.LENGTH_SHORT).show();
            return;
        }
        int checkRadio = mCategoryRadioGroup.getCheckedRadioButtonId();
        Task task = new Task();
        switch (checkRadio) {
            case R.id.radio_emergency:
                task.setCategory(Const.TaskCategory.emergency);
                break;
            case R.id.radio_important:
                task.setCategory(Const.TaskCategory.important);
                break;
            case R.id.radio_ordinary:
                task.setCategory(Const.TaskCategory.ordinary);
                break;
        }
        task.setTaskTitle(title);
        task.setTaskDescribe(describe);
        task.setState(Const.TaskState.unFinished);
        task.setUpdateTime(System.currentTimeMillis());
        saveTask(task);
    }

    private void saveTask(Task task) {
        if (task == null) {
            return;
        }
        DBManager.getInstance().getDaoSession().getTaskDao().save(task);
        EventBus.getDefault().post(new RefreshTask(task.getCategory()));
        finish();
    }
}
