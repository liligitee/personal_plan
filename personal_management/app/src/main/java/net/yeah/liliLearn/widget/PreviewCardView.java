package net.yeah.liliLearn.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;

import net.yeah.liliLearn.R;

public class PreviewCardView extends CardView {
    private static final int MAX_ITEM_NUMBER = 5;
    private ListView mListView;

    public PreviewCardView(@NonNull Context context) {
        this(context, null);
    }

    public PreviewCardView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PreviewCardView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        View view = LayoutInflater.from(context).inflate(R.layout.view_preview_card_layout, this);
        initView(view);
    }

    private void initView(View view) {

    }

    public void setAdapter(ListAdapter adapter) {
        mListView.setAdapter(adapter);
    }
}
