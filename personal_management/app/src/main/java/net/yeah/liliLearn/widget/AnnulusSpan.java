package net.yeah.liliLearn.widget;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.style.LineBackgroundSpan;

import net.yeah.liliLearn.base.App;
import net.yeah.liliLearn.utils.DensityUtil;

public class AnnulusSpan implements LineBackgroundSpan {
    @Override
    public void drawBackground(Canvas c, Paint p, int left, int right, int top, int baseline, int bottom, CharSequence text, int start, int end, int lnum) {
        Paint paint = new Paint();
        paint.setAntiAlias(true); //消除锯齿
        paint.setStyle(Paint.Style.STROKE);//绘制空心圆或 空心矩形
        int ringWidth = dip2px(1);//圆环宽度
        //绘制圆环
        paint.setColor(Color.parseColor("#00bcbe"));
        paint.setStrokeWidth(ringWidth);
        c.drawCircle((right - left) / 2, (bottom - top) / 2, dip2px(18), paint);
    }

    private int dip2px(float dpValue) {
        return DensityUtil.dip2px(App.getInstance().getApplicationContext(), dpValue);
    }
}