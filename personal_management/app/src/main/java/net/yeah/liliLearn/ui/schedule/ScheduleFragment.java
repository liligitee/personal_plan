package net.yeah.liliLearn.ui.schedule;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import net.yeah.liliLearn.R;
import net.yeah.liliLearn.base.BaseFragment;
import net.yeah.liliLearn.widget.EventDecorator;

import java.util.ArrayList;
import java.util.List;

public class ScheduleFragment extends BaseFragment {
    private MaterialCalendarView mCalendarView;
    private RecyclerView mRecyclerView;
    private NestedScrollView mScrollView;

    public static ScheduleFragment newInstance() {
        Bundle args = new Bundle();
        ScheduleFragment fragment = new ScheduleFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_schedule, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        setEvent();
    }

    private void initView(View view) {
        mCalendarView = view.findViewById(R.id.calendar_view);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mScrollView = view.findViewById(R.id.scroll_view);

        //设置周的文本
        mCalendarView.setWeekDayLabels(new String[]{"日", "一", "二", "三", "四", "五", "六"});

        List<CalendarDay> calendarDays = new ArrayList<>();
        final CalendarDay day = CalendarDay.from(2019, 1, 16);
        calendarDays.add(day);
        mCalendarView.addDecorator(new EventDecorator(calendarDays));


    }

    private void setEvent() {
        mCalendarView.setOnDateChangedListener((materialCalendarView, calendarDay, b) -> {
            Log.e("s", mCalendarView.getSelectedDate() + "");
        });
        mScrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (nestedScrollView, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (oldScrollY < scrollY && ((scrollY - oldScrollY) > 15)) {// 向上
                Log.e("wangly", "距离：" + (oldScrollY < scrollY) + "---" + (scrollY - oldScrollY));
                Log.e("TAG", "向上滑动");

            } else if (oldScrollY > scrollY && (oldScrollY - scrollY) > 15) {// 向下
                Log.e("wangly", "距离：" + (oldScrollY > scrollY) + "---" + (oldScrollY - scrollY));
                Log.e("TAG", " 向下滑动");

            }
        });
    }
}
