package net.yeah.liliLearn.ui.task.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;

import net.yeah.liliLearn.R;
import net.yeah.liliLearn.base.BaseActivity;
import net.yeah.liliLearn.constants.Const;
import net.yeah.liliLearn.ui.task.adapter.TaskFragmentAdapter;
import net.yeah.liliLearn.ui.task.fragment.TaskListFragment;

import java.util.ArrayList;
import java.util.List;

public class TaskListActivity extends BaseActivity {
    private String[] titles = new String[]{"所有任务", "已完成", "未完成"};
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list);
        initView();
        initAdapter();
        setEvent();
    }

    private void initView() {
        mTabLayout = findViewById(R.id.tab_layout);
        mViewPager = findViewById(R.id.view_pager);
        mToolbar = findViewById(R.id.toolbar);
    }

    private void initAdapter() {
        for (int i = 0; i < titles.length; i++) {
            mTabLayout.addTab(mTabLayout.newTab());
        }

        List<Fragment> fragments = new ArrayList<>();
        fragments.add(TaskListFragment.newInstance(Const.TaskState.all));
        fragments.add(TaskListFragment.newInstance(Const.TaskState.finished));
        fragments.add(TaskListFragment.newInstance(Const.TaskState.unFinished));
        TaskFragmentAdapter adapter = new TaskFragmentAdapter(getSupportFragmentManager(), fragments);
        mViewPager.setAdapter(adapter);
        mTabLayout.setupWithViewPager(mViewPager, false);
        for (int i = 0; i < titles.length; i++) {
            mTabLayout.getTabAt(i).setText(titles[i]);
        }
    }

    private void setEvent() {
        mToolbar.setNavigationOnClickListener(v -> finish());
    }
}
