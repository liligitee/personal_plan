package net.yeah.liliLearn.ui.task.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.yeah.liliLearn.R;
import net.yeah.liliLearn.entity.Task;
import net.yeah.liliLearn.ui.task.activity.TaskDetailsActivity;
import net.yeah.liliLearn.widget.TaskItemView;

import java.util.List;

public class TaskCardAdapter extends RecyclerView.Adapter<TaskCardAdapter.TaskItemViewHolder> {
    private List<Task> taskList;
    private Context mContext;

    public TaskCardAdapter(Context mContext, List<Task> taskList) {
        this.taskList = taskList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public TaskItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_task_card_item, viewGroup, false);
        return new TaskItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskItemViewHolder viewHolder, int position) {
        Task task = taskList.get(position);
        viewHolder.setData(task);
        viewHolder.setEvent(task);
    }

    @Override
    public int getItemCount() {
        return taskList == null ? 0 : taskList.size();
    }

    class TaskItemViewHolder extends RecyclerView.ViewHolder {
        TaskItemView taskItemView;

        public TaskItemViewHolder(View itemView) {
            super(itemView);
            taskItemView = itemView.findViewById(R.id.task_item_view);
        }

        public void setData(Task task) {
            taskItemView.setData(task);
        }

        public void setEvent(Task task) {
            taskItemView.setOnClickListener(v -> {
                Intent intent = new Intent(mContext, TaskDetailsActivity.class);
//                intent.putExtra("taskId", task.getTId());
                mContext.startActivity(intent);
            });
        }
    }
}
