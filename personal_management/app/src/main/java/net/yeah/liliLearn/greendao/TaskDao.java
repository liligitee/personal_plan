package net.yeah.liliLearn.greendao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

import net.yeah.liliLearn.entity.Task;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "TASK".
*/
public class TaskDao extends AbstractDao<Task, Long> {

    public static final String TABLENAME = "TASK";

    /**
     * Properties of entity Task.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property TaskId = new Property(0, Long.class, "taskId", true, "_id");
        public final static Property TaskTitle = new Property(1, String.class, "taskTitle", false, "TASK_TITLE");
        public final static Property TaskDescribe = new Property(2, String.class, "taskDescribe", false, "TASK_DESCRIBE");
        public final static Property Category = new Property(3, int.class, "category", false, "CATEGORY");
        public final static Property State = new Property(4, int.class, "state", false, "STATE");
        public final static Property UpdateTime = new Property(5, Long.class, "updateTime", false, "UPDATE_TIME");
    }


    public TaskDao(DaoConfig config) {
        super(config);
    }
    
    public TaskDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"TASK\" (" + //
                "\"_id\" INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: taskId
                "\"TASK_TITLE\" TEXT NOT NULL ," + // 1: taskTitle
                "\"TASK_DESCRIBE\" TEXT NOT NULL ," + // 2: taskDescribe
                "\"CATEGORY\" INTEGER NOT NULL ," + // 3: category
                "\"STATE\" INTEGER NOT NULL ," + // 4: state
                "\"UPDATE_TIME\" INTEGER NOT NULL );"); // 5: updateTime
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"TASK\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, Task entity) {
        stmt.clearBindings();
 
        Long taskId = entity.getTaskId();
        if (taskId != null) {
            stmt.bindLong(1, taskId);
        }
        stmt.bindString(2, entity.getTaskTitle());
        stmt.bindString(3, entity.getTaskDescribe());
        stmt.bindLong(4, entity.getCategory());
        stmt.bindLong(5, entity.getState());
        stmt.bindLong(6, entity.getUpdateTime());
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, Task entity) {
        stmt.clearBindings();
 
        Long taskId = entity.getTaskId();
        if (taskId != null) {
            stmt.bindLong(1, taskId);
        }
        stmt.bindString(2, entity.getTaskTitle());
        stmt.bindString(3, entity.getTaskDescribe());
        stmt.bindLong(4, entity.getCategory());
        stmt.bindLong(5, entity.getState());
        stmt.bindLong(6, entity.getUpdateTime());
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public Task readEntity(Cursor cursor, int offset) {
        Task entity = new Task( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // taskId
            cursor.getString(offset + 1), // taskTitle
            cursor.getString(offset + 2), // taskDescribe
            cursor.getInt(offset + 3), // category
            cursor.getInt(offset + 4), // state
            cursor.getLong(offset + 5) // updateTime
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, Task entity, int offset) {
        entity.setTaskId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setTaskTitle(cursor.getString(offset + 1));
        entity.setTaskDescribe(cursor.getString(offset + 2));
        entity.setCategory(cursor.getInt(offset + 3));
        entity.setState(cursor.getInt(offset + 4));
        entity.setUpdateTime(cursor.getLong(offset + 5));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(Task entity, long rowId) {
        entity.setTaskId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(Task entity) {
        if(entity != null) {
            return entity.getTaskId();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(Task entity) {
        return entity.getTaskId() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
}
