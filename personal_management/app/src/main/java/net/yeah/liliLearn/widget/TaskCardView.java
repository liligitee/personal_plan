package net.yeah.liliLearn.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import net.yeah.liliLearn.R;

public class TaskCardView extends CardView {
    // attribute
    private String headerText;
    private int headerColor;
    // Views
    private View mHeaderColorView;
    private TextView mHeaderTextView;
    private EmptyRecyclerView mRecyclerView;
    private TaskCardEmptyView mEmptyView;

    public TaskCardView(Context context) {
        this(context, null);
    }

    public TaskCardView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TaskCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TaskCardView, 0, 0);
        try {
            headerText = a.getString(R.styleable.TaskCardView_headerText);
            headerColor = a.getColor(R.styleable.TaskCardView_headerColor, 0);
        } finally {
            a.recycle();
        }
        View view = LayoutInflater.from(context).inflate(R.layout.view_task_card_layout, this);
        initView(view);
    }

    private void initView(View view) {
        mHeaderColorView = view.findViewById(R.id.header_color_view);
        mHeaderTextView = view.findViewById(R.id.header_text_view);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mEmptyView = view.findViewById(R.id.empty_view);


        mHeaderTextView.setText(headerText);
        mHeaderColorView.setBackgroundColor(headerColor);
        initRecycler();
    }

    private void initRecycler() {
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setEmptyView(mEmptyView);
    }

    public void setEmptyViewClickListener(String title, OnClickListener listener) {
        mEmptyView.setOnAddClickListener(title, listener);
    }

    public void setAdapter(@NonNull RecyclerView.Adapter adapter) {
        mRecyclerView.setAdapter(adapter);
    }
}
