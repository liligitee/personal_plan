package net.yeah.liliLearn.ui.task.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.yeah.liliLearn.R;
import net.yeah.liliLearn.base.BaseFragment;
import net.yeah.liliLearn.constants.AnnotationConst;
import net.yeah.liliLearn.constants.Const;
import net.yeah.liliLearn.entity.Task;
import net.yeah.liliLearn.greendao.TaskDao;
import net.yeah.liliLearn.ui.task.adapter.TaskListAdapter;
import net.yeah.liliLearn.utils.DBManager;
import net.yeah.liliLearn.widget.EmptyRecyclerView;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.List;

public class TaskListFragment extends BaseFragment {
    private static final String ARGS_STATE_KEY = "args_state_key";

    private EmptyRecyclerView mRecyclerView;
    private TextView mEmptyView;

    private int taskState = Const.TaskState.all;
    private List<Task> taskList = new ArrayList<>();
    private TaskListAdapter adapter;

    public static TaskListFragment newInstance(@AnnotationConst.TaskState int state) {
        Bundle args = new Bundle();
        args.putInt(ARGS_STATE_KEY, state);
        TaskListFragment fragment = new TaskListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_task_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            taskState = getArguments().getInt(ARGS_STATE_KEY);
        }
        initView(view);
        initAdapter();
        fetchTaskList(taskState);
    }

    private void initView(View view) {
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mEmptyView = view.findViewById(R.id.empty_view);
    }

    private void initAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        adapter = new TaskListAdapter(getContext(), taskList);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setEmptyView(mEmptyView);
    }

    private void fetchTaskList(int state) {
        taskList.clear();
        QueryBuilder<Task> queryBuilder = DBManager
                .getInstance()
                .getDaoSession()
                .getTaskDao()
                .queryBuilder();
        if (state == Const.TaskState.all) {
            taskList.addAll(queryBuilder.build().list());
        } else {
            taskList.addAll(queryBuilder.where(TaskDao.Properties.State.eq(state)).build().list());
        }
        adapter.notifyDataSetChanged();
    }
}
