package net.yeah.liliLearn.ui.task.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.yeah.liliLearn.R;
import net.yeah.liliLearn.entity.Task;

import java.util.List;

public class TaskListAdapter extends RecyclerView.Adapter<TaskListAdapter.TaskItemViewHolder> {

    private List<Task> taskList;
    private Context mContext;

    public TaskListAdapter(Context mContext, List<Task> taskList) {
        this.taskList = taskList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public TaskItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_task_list_item, viewGroup, false);
        return new TaskItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskItemViewHolder taskItemViewHolder, int position) {
        Task task = taskList.get(position);
        taskItemViewHolder.setData(task);
    }


    @Override
    public int getItemCount() {
        return taskList.size();
    }

    class TaskItemViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView describe;
        private TextView updateTime;

        public TaskItemViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            describe = itemView.findViewById(R.id.describe);
            updateTime = itemView.findViewById(R.id.update_time);
        }

        public void setData(Task task) {
            title.setText(task.getTaskTitle());
            describe.setText(task.getTaskDescribe());
//            updateTime.setText(task.getUpdateTime().toString());
        }
    }
}
