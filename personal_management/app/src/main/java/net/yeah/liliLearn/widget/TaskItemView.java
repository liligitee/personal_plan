package net.yeah.liliLearn.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.yeah.liliLearn.R;
import net.yeah.liliLearn.entity.Task;

public class TaskItemView extends LinearLayout {
    private TextView mTaskOrderNumber;
    private TextView mTaskName;
    private CheckBox mTaskStateView;

    public TaskItemView(Context context) {
        this(context, null);
    }

    public TaskItemView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TaskItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        View view = LayoutInflater.from(context).inflate(R.layout.view_task_item_laoyut, this);
        initView(view);
    }

    private void initView(View view) {
        mTaskOrderNumber = view.findViewById(R.id.task_order_number);
        mTaskName = view.findViewById(R.id.task_name);
        mTaskStateView = view.findViewById(R.id.task_state_view);
    }

    public void setData(Task task) {
        mTaskOrderNumber.setText(String.valueOf(task.getTaskId()));
        mTaskName.setText(task.getTaskTitle());
    }
}
