package net.yeah.liliLearn.ui.home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import net.yeah.liliLearn.R;
import net.yeah.liliLearn.base.BaseFragment;

public class HomeFragment extends BaseFragment {
    private ListView mListView;

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        initAdapter();
    }

    private void initView(View view) {
        mListView = view.findViewById(R.id.list_view);
    }

    private void initAdapter() {
//        TaskPreviewAdapter adapter = new TaskPreviewAdapter();
//        mListView.setAdapter(adapter);
    }
}
